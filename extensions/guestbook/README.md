Assignment to learn about and practice with TYPO3 v10

For further information, ask the author.

Installation: 
    
Run 'composer install' in root directory
Run 'npm install' in Build/Sass directory
    Run 'npm run scss' in Build/Sass
        
Run 'ddev config' to configure ddev
Import database dump found at Build/db.sql.gz in ddev
Run 'ddev start'