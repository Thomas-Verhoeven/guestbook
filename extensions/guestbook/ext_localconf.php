<?php
defined('TYPO3_MODE') || die('Access denied.');

    // Deprecated; Changed to use of typenum
    // $GLOBALS['TYPO3_CONF_VARS']['FE']['eID_include']['ajax_page'] = \MyVendor\Guestbook\Controller\MyEidController::class . '::deleteMessage';

    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:guestbook/Configuration/TsConfig/Guestbook.tsconfig">');

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'MyVendor.Guestbook',
        'ShowContent',
        [
            // 'Controller' => 'Action'
            'Content' => 'showContent'
        ],
        // non-cacheable actions
        []
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'MyVendor.Guestbook',
        'FormValidation',
        [
            'Form' => 'validateForm'
        ],
        [
            'Form' => 'validateForm'
        ]
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'MyVendor.Guestbook',
        'editMessage',
        [
            'Message' => 'editMessage'
        ],
        [
            'Message' => 'editMessage'
        ]
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'MyVendor.Guestbook',
        'deleteMessage',
        [
            'Message' => 'deleteMessage'
        ],
        [
            'Message' => 'deleteMessage'
        ]
    );