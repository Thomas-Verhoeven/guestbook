<?php

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'MyVendor.Guestbook',
    'ShowContent',
    'Guestbook practice - Show Content',
    'EXT:guestbook/Resources/Public/Icons/Extension.svg'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'MyVendor.Guestbook',
    'Form Validation',
    'Guestbook practice - Form Validation',
    'EXT:guestbook/Resources/Public/Icons/Extension.svg'
);