<?php
namespace MyVendor\Guestbook\Controller;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use MyVendor\Guestbook\Domain\Repository\MessageRepository;
use MyVendor\Guestbook\Domain\Model\Message;

class MessageController extends ActionController
{
    /**
     * @var MessageRepository
     */
    private $messageRepository;

    /**
     * Inject the message repository
     *
     * @param \MyVendor\Guestbook\Domain\Repository\MessageRepository $messageRepository
     */
    public function injectProductRepository(MessageRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
    }

    public function addMessageAction($name, $messageContent) {

//        $messages = $this->messageRepository->findAll();
//        $this->view->assign('newMessage', 'Test');
//        $this->view->assign('messages', $messages);

        $message = new Message($name, $messageContent);
        $this->messageRepository->add($message);
        return true;
    }

    public function editMessageAction($uid) {
        $data = print_r($this->request);
        $response = json_encode('Message has been deleted');
        $response = json_encode($data);
        return $response;
    }

    public function deleteMessageAction()
    {
        $test = $this->request->getRequestUri();
        preg_match("/delete=([0-9]+)/", $test, $matches);
        if (!key_exists(1, $matches)) {
            return json_encode('Message not found');
        }
        $uid = $matches[1];

        GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_guestbook_domain_model_message')
            ->delete(
                'tx_guestbook_domain_model_message', // from
                [ 'uid' => (int)$uid ] // where
        );

        $response = json_encode('Message has been deleted');
        return $response;
    }
}

