<?php

namespace MyVendor\Guestbook\Controller;

use MyVendor\Guestbook\Domain\Repository\MessageRepository;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/**
 * Class ContentController
 *
 * @package MyVendor\Guestbook\Controller
 */
class ContentController extends ActionController
{

    /**
     * @var MessageRepository
     */
    private $messageRepository;

    /**
     * Inject the message repository
     *
     * @param \MyVendor\Guestbook\Domain\Repository\MessageRepository $messageRepository
     */
    public function injectProductRepository(MessageRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
    }

    /**
     * List Action
     *
     * @return void
     */
    public function showContentAction()
    {
//        $arguments = $this->request->getArguments();
//        echo '<pre>';
//        var_dump($arguments);
//        $newMessage = $arguments['newMessage'] ?? 'Geen newMessage';
//        var_dump($newMessage);
//        $this->view->assign('newMessage', $newMessage);
        $this->listMessages();
//        $this->TestGuestbook();
    }

    public function listMessages() {
        $messages = $this->messageRepository->findAll();
//        die('Message.php');

//        var_dump($messages[0]);
//        echo '<br>Hoi';
//        echo '<pre>';
//        var_dump($messages);
//        die();
        $this->view->assign('messages', $messages);
    }

    public function TestGuestbook() {
        $this->view->assign('TestGuestbook', 'Hey hoi');
    }
}
