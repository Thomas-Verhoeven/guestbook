<?php
namespace MyVendor\Guestbook\Controller;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Http\JsonResponse;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;

/**
 * Class MyEidController
 * @package MyVendor\Guestbook\Controller
 *
 * Deprecated
 * Choice of eID over form submission came from educational reasons.
 * Practical application is low since it doesn't refresh the page to indicate changes
 *
 * Use of eID has been replaced with typenum. This class only serves as future reference
 */

class MyEidController
{

    /**
     * @param ResponseInterface $response
     */
    public function deleteMessage(ServerRequestInterface $request) : ResponseInterface
    {
        if (is_int($toBeDeletedMessage = $request->getQueryParams()['delete'])) {
            GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_guestbook_domain_model_message')
                ->delete(
                    'tx_guestbook_domain_model_message', // from
                    [ 'uid' => (int)$toBeDeletedMessage ] // where
                );
        }
//        return 'Hoi';
//        echo '<pre>';
//        var_dump($request->getQueryParams());
//        echo $toBeDeletedMessage;
//        die();
//        $responseData =   'Hello world';
//        $response = new Response($responseData, 200);
        $response = new JsonResponse();
        return $response;
    }
}
