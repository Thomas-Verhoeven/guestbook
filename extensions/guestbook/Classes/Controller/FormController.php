<?php

namespace MyVendor\Guestbook\Controller;

use MyVendor\Guestbook\Domain\Model\Message;
use MyVendor\Guestbook\Domain\Repository\MessageRepository;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

/**
 * Class FormController
 *
 * @package MyVendor\Guestbook\Controller
 */
class FormController extends ActionController
{

    /**
     * @var MessageRepository
     */
    private $messageRepository;

    /**
     * @var MessageController
     */
    private $messageController;

    /**
     * Inject the message repository
     *
     * @param \MyVendor\Guestbook\Domain\Repository\MessageRepository $messageRepository
     */
    public function injectMessageRepository(MessageRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
    }

    /**
     * Inject the message controller
     *
     * @param \MyVendor\Guestbook\Controller\MessageController $messageController
     */
    public function injectMessageController(MessageController $messageController) {
        $this->messageController = $messageController;
    }

    public function validateFormAction() {

        $request = $this->request->getArguments();
        $newMessage = false;
        if (isset($request['guestbook']['name']) && isset($request['guestbook']['messageContent'])) {
            $name = $request['guestbook']['name'];
            $messageContent = $request['guestbook']['messageContent'];
            $newMessage = $this->messageController->addMessageAction($name, $messageContent);
        }
        $this->view->assign('newMessage', $newMessage);
    }
}
