<?php

namespace MyVendor\Guestbook\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

class Message extends AbstractEntity
{

    /**
     * The unique ID of the message
     *
     * @var integer
     */
    protected $uid;

    /**
     * The name of the commenter
     *
     * @var string
     **/
    protected $name;

    /**
     * The message content in the guestbook
     *
     * @var string
     **/
    protected $messageContent;

    /**
     * The date of the message
     *
     * @var string
     **/
    protected $date;

    /**
     * Message constructor.
     *
     * @param string $name
     * @param string $messageContent
     * @param string $date
     */
    public function __construct($name = 'DefaultName', $messageContent = 'DefaultMessage', $date = NULL)
    {
        $date = $date ?? (new \DateTime())->format('Y-m-d H:i:s');
        $this->setName($name);
        $this->setMessageContent($messageContent);
        $this->setDate($date);
    }

    /**
     * Sets the name of the commenter
     *
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * Gets the name of the commenter
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the message content
     *
     * @param string $messageContent
     */
    public function setMessageContent(string $messageContent)
    {
        $this->messageContent = $messageContent;
    }

    /**
     * Gets the message content
     *
     * @return string
     */
    public function getMessageContent()
    {
        return $this->messageContent;
    }

    /**
     * Sets the date of the comment
     *
     * @param string $date
     */
    public function setDate(string $date)
    {
        $this->date = $date;
    }

    /**
     * Gets the date of the comment
     *
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

}
